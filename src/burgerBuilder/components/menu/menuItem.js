import React from 'react'

const MenuItem = props => {
  let buttonClasses = ['less'];

  if (props.noIngredient) {
    buttonClasses.push('disabled');
	}

	return (
		<div className="row">
			<span>{props.name}</span>
			<button onClick={() => props.remove(props.name)} className={buttonClasses.join(' ')}>Less</button>
			<button onClick={() => props.add(props.name)} className="more">More</button>
		</div>
	)
};

export default MenuItem;